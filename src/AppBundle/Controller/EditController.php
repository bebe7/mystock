<?php

namespace AppBundle\Controller;

use BackBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Form\ProductType;

class EditController extends Controller
{

    /**
     * @Route("/add", name="add")
     */
    public function addAction(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product );
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $now = new \DateTime('now');
            $product->setCreateDate($now);
            $product->setUpdateDate($now);

            // to DB
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('details', array('id'=>$product->getId()));
        }

        return $this->render('add.html.twig', array('form'=>$form->createView()));
    }


    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function editAction(Request $request, $id)
    {
        $product = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->find($id);

        $form = $this->createForm(ProductType::class, $product );
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            $now = new \DateTime('now');
            $product->setUpdateDate($now);

            // to DB
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('details', array('id'=>$id));
        }

        return $this->render('edit.html.twig', array('product'=>$product, 'form'=>$form->createView()));
    }


    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('BackBundle:Product')->find($id);

        $em->remove($product);
        $em->flush();

        return $this->redirectToRoute('products');
    }





}