<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FrontController extends Controller
{
    /**
     * @Route("/", name="home")
     */
    public function indexAction(Request $request)
    {
        $searchText = $request->get('search');

        $products = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->findLast(16);

        return $this->render('index.html.twig', array('products'=>$products, 'searchText'=>$searchText));
    }

    /**
     * @Route("/products", name="products")
     */
    public function productsAction(Request $request)
    {
        $searchText = $request->get('search');

        $query = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->searchQuery($searchText);

        $paginator = $this->get('knp_paginator');

        $pagination = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            30,
            array('defaultSortFieldName' => 'p.id', 'defaultSortDirection' => 'desc')
        );

        return $this->render('products.html.twig',array('products'=>$pagination, 'searchText'=>$searchText));
    }

    /**
     * @Route("/details/{id}", name="details")
     */
    public function detailsAction($id)
    {
        $product = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->find($id);
        
        return $this->render('details.html.twig', array('product'=>$product));
    }

}
