<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class ProductType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $category = array(
            'class' => 'BackBundle:Category',
            'multiple' => false,
            'expanded' => true,
        );

        $tags = array(
            'class' => 'BackBundle:Tag',
            'multiple' => true,
            'expanded' => true,
        );


        $builder
            ->add('name',TextType::class)
            ->add('thumbnail',TextType::class)
            ->add('category',EntityType::class, $category)
            ->add('tags',EntityType::class, $tags)
            ->add('description', TextareaType::class);


    }

}