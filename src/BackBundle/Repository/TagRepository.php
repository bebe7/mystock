<?php

namespace BackBundle\Repository;


class TagRepository extends \Doctrine\ORM\EntityRepository
{


    public function findIdName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT tag.id, tag.name FROM BackBundle:Tag tag ORDER BY tag.id DESC'
            )
            ->getResult();
    }


    
}
