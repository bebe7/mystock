<?php

namespace BackBundle\Repository;

/**
 * CategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CategoryRepository extends \Doctrine\ORM\EntityRepository
{

    public function findIdName()
    {
        return $this->getEntityManager()
            ->createQuery(
                'SELECT cat.id, cat.name FROM BackBundle:Category cat ORDER BY cat.id DESC'
            )
            ->getResult();
    }

}
