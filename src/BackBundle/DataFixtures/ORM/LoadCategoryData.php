<?php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use BackBundle\Entity\Category;
use Faker\Factory;


class LoadCategoryData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // use the factory to create a Faker\Generator instance
        $faker = Factory::create();

        $names = array('', 'banners', 'themes', 'templates', 'landing pages', 'buttons');

        for ($i=1; $i <= 5; $i++) {

            $cat = new Category();
            $cat->setName( $names[$i] );
            $cat->setDescription( $faker->text(200) );

            $manager->persist($cat);
            $this->addReference('cat-'.$i, $cat);
            
            //echo "Create Category: " . $cat->getName() . "\n";
        }
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }

}