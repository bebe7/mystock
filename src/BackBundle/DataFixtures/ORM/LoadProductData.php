<?php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

use Doctrine\Common\Persistence\ObjectManager;

use BackBundle\Entity\Product;

use Faker\Factory;


class LoadProductData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        // use the factory to create a Faker\Generator instance
        $faker = Factory::create();

        $temp = array(0,1,2,3,4,5,6,7,8,9,10);

        $tempThumb = array();

        for ($i=1; $i <= 45; $i++) {
            if($i<10) {
                array_push($tempThumb, '00'. $i.'.jpg' );
            }
            else if ($i<100) {
                array_push($tempThumb, '0'. $i.'.jpg' );
            }
            else {
                array_push($tempThumb, $i.'.jpg' );
            }
        }


        for ($i=1; $i <= 125; $i++) {

            $prod = new Product();
            $prod->setName( $faker->text(30)  );
            $prod->setDescription( $faker->text(250) );
            $prod->setDownloaded( $faker->numberBetween(0, 1000) );
            $prod->setThumbnail( $tempThumb[rand(0,44)] );

            $prod->setCreateDate( $faker->dateTimeThisMonth() );
            $prod->setUpdateDate( $faker->dateTimeThisMonth() );

            $catNumber = rand(0, 5);
            if($catNumber) {
                $cat = $this->getReference('cat-'.$catNumber);
                $prod->setCategory($cat);
            }

            $tagsId = array_rand($temp, rand(2, 7));
            foreach ($tagsId as &$id) {
                if($id == 0)
                {
                    break;
                }
                else {
                    $tag = $this->getReference('tag-'. $id);
                    $prod->addTag( $tag );
                }
            }

            $manager->persist($prod);

            //echo "Create Product: " . $prod->getName() . "\n";
        }
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }

}