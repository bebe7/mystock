<?php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use BackBundle\Entity\Tag;

class LoadTagData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $colors  = array('', 'black', 'brown', 'red', 'orange', 'yellow', 'green', 'blue', 'purple', 'gray', 'white' );

        for ($i=1; $i <= 10; $i++) {

            $tag = new Tag();
            $tag->setName( $colors[$i] );

            $manager->persist($tag);
            $this->addReference('tag-'.$i, $tag);

            //echo "Create Tag: " . $tag->getName() . "\n";
        }
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }

}