<?php

namespace BackBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

use BackBundle\Entity\User;


class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {

        $user = new User();
        $user->setUsername('demo');
        $user->setPassword('demo');
        $user->setEmail('demo@briz.pl');
        $user->setIsActive(true);

        $manager->persist($user);

        echo "Create User: " . $user->getUsername() . "\n";
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 4;
    }

}