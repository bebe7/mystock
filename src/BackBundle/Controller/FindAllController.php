<?php

namespace BackBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class FindAllController extends Controller
{

    /**
     * @Route("/products", name="back_products")
     */
    public function productsAction()
    {

        $products = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->findAll();

        return $this->render('BackBundle:allEntity:products.html.twig',
            array_merge(['products'=>$products], $this->get('search')->getData()));
    }

    /**
     * @Route("/categories", name="back_categories")
     */
    public function categoriesAction()
    {
        $categories = $this->getDoctrine()
            ->getRepository('BackBundle:Category')
            ->findAll();

        return $this->render('BackBundle:allEntity:categories.html.twig',
            array_merge(['categories'=>$categories], $this->get('search')->getData()));
    }

    /**
     * @Route("/tags", name="back_tags")
     */
    public function tagsAction()
    {
        $tags = $this->getDoctrine()
            ->getRepository('BackBundle:Tag')
            ->findAll();

        return $this->render('BackBundle:allEntity:tags.html.twig',
            array_merge(['tags'=>$tags], $this->get('search')->getData()));
    }

    /**
     * @Route("/users", name="back_users")
     */
    public function usersAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('BackBundle:User')
            ->findAll();

        return $this->render('BackBundle:allEntity:users.html.twig', array('users'=>$users));
    }

    /**
     * @Route("/products-tags", name="back_products-tags")
     */
    public function products_tagsAction()
    {
        $conn = $this->get('database_connection');
        $products_tags = $conn->fetchAll('SELECT * FROM product_tag ORDER BY product_id DESC');

        return $this->render('BackBundle:allEntity:products_tags.html.twig', array('products_tags'=>$products_tags));
    }

}
