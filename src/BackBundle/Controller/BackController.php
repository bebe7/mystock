<?php

namespace BackBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class BackController extends Controller
{

    /**
     * @Route("/", name="back_home")
     */
    public function indexAction()
    {
        return $this->render('BackBundle:back:index.html.twig');
    }

    /**
     * @Route("/thumbnails", name="back_thumbnails")
     */
    public function thumbnailAction()
    {
        return $this->render('BackBundle:back:thumbnails.html.twig');
    }


    /**
     * @Route("/search", name="back_search")
     */
    public function searchAction(Request $request)
    {
        $searchText = $request->get('searchText');

        $products = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->search($searchText);

        return $this->render('BackBundle:back:loop.html.twig',
            array_merge(['products'=>$products],
                        ['searchText' => $searchText],
                        $this->get('search')->getData()));
    }



    /**
     * @Route("/product/{id}", name="back_product")
     */
    public function productAction($id)
    {
        $product = $this->getDoctrine()
            ->getRepository('BackBundle:Product')
            ->find($id);

        return $this->render('BackBundle:oneEntity:product.html.twig', array('product'=>$product));
    }
    
    /**
     * @Route("/category/{id}", name="back_category")
     */
    public function categoryAction($id)
    {
        $cat = $this->getDoctrine()
            ->getRepository('BackBundle:Category')
            ->find($id);

        return $this->render('BackBundle:oneEntity:category.html.twig', array('category'=>$cat));
    }

    /**
     * @Route("/tag/{id}", name="back_tag")
     */
    public function tagAction($id)
    {
        $tag = $this->getDoctrine()
            ->getRepository('BackBundle:Tag')
            ->find($id);

        return $this->render('BackBundle:oneEntity:tag.html.twig', array('tag'=>$tag));
    }

}
