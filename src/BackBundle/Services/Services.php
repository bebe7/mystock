<?php



namespace BackBundle\Services;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;

class Services
{

    protected $em;
    protected $requestStack;


    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public function hello()
    {
        return 'Hello World';
    }


    public function getData() {

        $request = $this->requestStack->getCurrentRequest();
        $searchText = $request->get('searchText');

        return array('searchText'=>$searchText, 'catList'=> $this->categories(), 'tagList'=>$this->tags());
    }

    public function categories() {

        $categories = $this->em
            ->getRepository('BackBundle:Category')
            ->findIdName();

        return $categories;
    }

    public function tags() {

        $tags = $this->em
            ->getRepository('BackBundle:Tag')
            ->findIdName();

        return $tags;
    }






}